import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.stage.Stage;
import javafx.scene.Group;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.util.Duration;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.KeyCode;






public class Executable extends Application {
    private Stage stagePrincipal ;
    private Pane paneAccueil;
    private Pane root;
    private Group caracteres;
    private GestionJeu gestionnaire;
    private int hauteurTexte;
    private int largeurCaractere;
    private Background imageFond = new Background( new BackgroundImage(new Image("file:img/BackGroundMars.png"), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT));


    public static void main(String[] args) {
        launch(args);
    }

    public Stage getStagePrincipal(){
        return this.stagePrincipal;
    }
    private void afficherCaracteres(){
        caracteres.getChildren().clear();
        int hauteur = (int) root.getHeight();
        for( ChainePositionnee c : gestionnaire.getChaines().chaines)
        {
            Text t = new Text (c.x*largeurCaractere,hauteur - c.y*hauteurTexte, c.c);
            t.setFont(Font.font ("Monospaced", 10));
            if (c.toString()=="●"){
                t.setFill(Color.RED);
            }
            if (c.toString()=="┃"){
                t.setFill(Color.GREENYELLOW);
            }
            caracteres.getChildren().add(t);
        }
    }

    private void lancerAnimation() {
        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(0),
                    new EventHandler<ActionEvent>() {
                        @Override public void handle(ActionEvent actionEvent) {
                            gestionnaire.jouerUnTour();   
                            afficherCaracteres();
                        }
                    }),
                new KeyFrame(Duration.seconds(0.025))
                );
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    public void GameOver( int score, int nbAliensRestants, boolean depassement){
        
        VBox boite = new VBox();
        boite.setBackground(imageFond);
        boite.setAlignment(Pos.CENTER);
        boite.setSpacing(20) ;
        Text texte = new Text();
        if (depassement){
            texte = new Text("Vous avez perdu ! Les aliens se sont approché de votre vaisseau\net l'ont détruit !");
        }
        else{
            texte = new Text("Vous avez perdu ! Il restait "+nbAliensRestants+" aliens à éliminer :(");
        }
        Text scoreFinal = new Text("Votre Score : "+score);
        texte.setFont(Font.font("Arial", 20));
        Button quitter = new Button("Quitter");
        quitter.setOnAction(e -> {Platform.exit();});
        boite.getChildren().addAll(texte, scoreFinal, quitter); 
        stagePrincipal.setTitle("IUT Space Invader - Défaite");
        Scene scene = new Scene(boite, 600,700);

        this.stagePrincipal.setScene(scene);
        this.stagePrincipal.show();
        
    
    }

    public void Victoire(int score){
        VBox boite = new VBox();
        boite.setBackground(imageFond);
        boite.setAlignment(Pos.CENTER);
        boite.setSpacing(20) ;
        Text texte = new Text("Vous avez gagné ! Bravo !");
        Text scoreFinal = new Text("Votre Score : "+score);
        texte.setFont(Font.font("Arial", 20));
        Button quitter = new Button("Quitter");
        quitter.setOnAction(e -> {Platform.exit();});
        boite.getChildren().addAll(texte, scoreFinal, quitter);  
        stagePrincipal.setTitle("IUT Space Invader - Victoire");
        Scene scene = new Scene(boite, 600,700);

        this.stagePrincipal.setScene(scene);
        this.stagePrincipal.show();
    }


        @Override
        public void start(Stage primaryStage) {
            this.stagePrincipal = new Stage();
            this.stagePrincipal.setTitle("IUTO Space Invader");
        
            ImageView background = new ImageView(new Image("file:img/Space-Invaders-Sans-Serif-Font-1.jpg", 600, 700, false, true));

            caracteres = new Group();
            paneAccueil = new AnchorPane();
            paneAccueil.getChildren().add(background);
            root= new AnchorPane(caracteres);
            root.setBackground(imageFond);
            gestionnaire = new GestionJeu(this);
            Text t=new Text("█");
            t.setFont(Font.font("Monospaced",10));
            hauteurTexte =(int) t.getLayoutBounds().getHeight();    
            largeurCaractere = (int) t.getLayoutBounds().getWidth();
            
            Scene scene = new Scene(root,gestionnaire.getLargeur()*largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);
            
            scene.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
                if(key.getCode()==KeyCode.LEFT)
                    gestionnaire.toucheGauche();
                if(key.getCode()==KeyCode.RIGHT)
                    gestionnaire.toucheDroite();
                if(key.getCode()==KeyCode.SPACE)
                    gestionnaire.toucheEspace();
            });

            Scene accueil = new Scene(paneAccueil,gestionnaire.getLargeur()*largeurCaractere,gestionnaire.getHauteur()*hauteurTexte);
            accueil.addEventHandler(KeyEvent.KEY_PRESSED,(key) -> {
                if (key.getCode()==KeyCode.SPACE){
                    this.stagePrincipal.setScene(scene);
                    lancerAnimation();
                }
            });
            

            this.stagePrincipal.setScene(accueil);
            this.stagePrincipal.setResizable(false);
            this.stagePrincipal.show();
          
        }
}
