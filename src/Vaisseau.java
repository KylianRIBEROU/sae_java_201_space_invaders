/**
* @param posX un double représentant la position en X du vaisseau
* @param vies un entier représentant les vies du vaisseau
*/
public class Vaisseau {
    
    private double posX;
    /**
     *
     */
    private int vies;

    /**
     * Constructeur de la classe Vaisseau qui crée un nouveau Vaisseau avec la position passée en paramètre et met le nombre de vies à 1
     * @param posX un double représentant la position en X du vaisseau 
     */
    public Vaisseau(double posX){
        this.posX=posX;
        this.vies=1;
    
        
    }
    /**
     * Méthode qui renvoie un double, la position en X du vaisseau
     * @return posX
     */
    public double getPosX(){
        return this.posX;
    }
    /**
     * Méthode qui renvoie un entier, le nombre de vies du vaisseau
     * @return vies
     */
    public int getVies(){
        return this.vies;
    }
    
    /**
     * Méthode qui fait perdre une vie au vaisseau. Elle décrémente la valeur de vies de 1
     */
    public void perdUneVie(){
        this.vies-=1;
    }
    /**
     * Méthode qui fait gagner une vie au vaisseau. Elle incrémente la valeur de vies de 1
     */
    public void gagneUneVIe(){
        this.vies+=1;
    }
    /**
     * Méthode qui renvoie un boolean, true si le nombre de vies du vaisseau est à 0, false sinon
     * @return boolean
     */
    public boolean aPerdu(){
        return this.vies==0;
    }
    /**
     * Méthode qui incrémente la position en X du vaisseau avec la valeur passée en paramètres
     * @param dx un double représentant la translation de la posX du vaisseau 
     */
    public void deplace(double dx){
        this.posX+=dx;
    }

    /**
     * Méthode qui renvoie un boolean, true si les positions passées en paramètres sont en collision avec celles du vaisseau,  false sinon
     * @param posX un double représentant la position en X
     * @param posY un double représentant la position en Y
     * @return boolean 
     */
    public boolean contient(double posX, double posY){  // le 14 est la taille de l'alien a l"horizontale
        if ( (posX >= this.getPosX() && posX <= this.getPosX()+14) && (posY >= 0 && posY <= 4)){
            return true;
        }
        return false;
    }

    /**
     * Méthode qui renvoie la position sur l'axe X correspondant au milieu du dessin du vaisseau, soit son canon
     * @return un double
     */
    public double getPositionCanon(){
        return this.getPosX()+("▄████████████▄".length()/2);
    }

    /**
     * Méthode qui renvoie le dessin à afficher sur la fenêtre de jeu des vies du vaisseau.
     * @return un ensemble de chaines contenant les vies du vaisseau 
     */
    public EnsembleChaines getViesVaisseauChaines(){
        EnsembleChaines vies = new EnsembleChaines();
        vies.ajouteChaine(1, 56, "Vies : "+this.getVies());
        return vies;
    }

    /**
     * Méthode qui renvoie l'ensemble des chaines du dessin du vaisseau.
     * @return EnsembleChaines : l'ensemble des chaînes du dessin du vaisseau avec leur position X et Y sur la fenêtre de jeu
     */
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines vaisseau = new EnsembleChaines();
        vaisseau.ajouteChaine(this.posX, 6, "       ▄      ");
        vaisseau.ajouteChaine(this.posX, 5, "      ███     ");
        vaisseau.ajouteChaine(this.posX, 4, "▄█████████████▄");   
        vaisseau.ajouteChaine(this.posX, 3, "███████████████");
        vaisseau.ajouteChaine(this.posX, 2, "███████████████"); 
        vaisseau.ajouteChaine(this.posX, 1, "               ");
        return vaisseau;
    }

 }
