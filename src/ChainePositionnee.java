/**
* @param x un double représentant la position en X de la chaine positionnée
* @param y un double représentant la position en Y de la chaine positionnée
* @param c un String représentant la chaine de caractères de la chaine positionnée 
*/
public class ChainePositionnee{
    double x,y;
    String c;

    /**
     * Constructeur de la classe ChainePositionnee. Cree un nouvel objet ChainePositionnee avec les attributs donnés en paramètres
     * @param a un double représentant la position en X de la chaine positionnée
     * @param b un double représentant la position en Y de la chaine positionnée 
     * @param d un String représentant la chaine de caractères de la chaine positionnée
     */
    public ChainePositionnee(double a,double b, String d){
        x=a;
        y=b;
        c=d;
    }
 
    /**
     * Méthode qui renvoie la chaine de caractères de la chaine positionnée
     */
    @Override
    public String toString(){
        return c;
    }
}
