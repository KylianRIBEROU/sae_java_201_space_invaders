import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

public class GestionJeu {
    public  int SCOREFINAL;
    private double hauteur;
    private double largeur;
    private Vaisseau vaisseau;
    private Projectile projectile;
    private Score score;
    private List<Alien> armeeAliens;
    private List<ProjectileAlien> tirsAliens;
    private Executable fenetresJFX;
    private boolean victoire;

    /**
     * Constructeur de la classe GestionJeu qui initialise toutes les variables nécessaires au fonctionnement du jeu
     * @param fenetre
     */
    public GestionJeu(Executable fenetre){
        this.hauteur=60;
        this.largeur=100;
        this.vaisseau = new Vaisseau(42);
        this.projectile=null;    
        this.score =new Score();
        this.armeeAliens=new ArrayList<>();
        this.tirsAliens=new ArrayList<>();
        creerlesAliens();
        this.fenetresJFX=fenetre;
        this.victoire = false;
    }

    /**
     * Méthode qui crée les aliens à afficher en les mettant dans la liste d'aliens avec une position correcte. Deux types d'aliens différents peuvent être 
     * ajoutés à la liste : 20% de chance d'un alien tireur et 80% d'un alien passif 
     */
    public void creerlesAliens(){
        this.armeeAliens= new ArrayList<>(); 
        double posX=8;
        double posY=50;                                 
        for (int i=0; i<10; i++){
            double nombre = Math.random();
            if (nombre > 0.8){
                this.armeeAliens.add(new AlienTireur(posX, posY));
            }
            else {
                this.armeeAliens.add(new AlienPassif(posX, posY));
            }
            posX+=17;
            if (posX > 85){
                posX=8;
                posY-=10;
            }
        }
    }

    /**
     * Méthode qui renvoie un double, la largeur de la fenêtre de jeu
     * @return largeur
     */
    public double getLargeur(){
        return this.largeur;
    }

    /**
     * Méthode qui renvoie un double, la hauteur de la fenêtre de jeu
     * @return hauteur
     */
    public double getHauteur(){
        return this.hauteur;
    }
    
    /**
     * Méthode qui renvoie un entier, le score associé au Score du jeu 
     * @return score
     */
    public int getScore(){
        return score.getScore();
    }

    /**
     * Méthode appelée lors de l'interaction de l'utilisateur avec la flèche gauche de son clavier. Sert à déplacer le vaisseau à gauche
     */
    public void toucheGauche(){
        if (vaisseau.getPosX()>2){
            this.vaisseau.deplace(-2);
        }
    }

    /**
     * Méthode appelée lors de l'interaction de l'utilisateur avec la flèche droite de son clavier. Sert à déplacer le vaisseau à droite
     */
    public void toucheDroite(){
        if (vaisseau.getPosX() < this.getLargeur()-18){
            this.vaisseau.deplace(2); // deplace le vaisseau a droite de deux unités
        }
    }

    /**
     * Méthode appelée lors de l'interaction de l'utilisateur avec la barre espace de son clavier. Sert à turer un projectile 
     */
    public void  toucheEspace(){        
        if (this.projectile==null){
            this.projectile=new Projectile(vaisseau.getPositionCanon(), 7);
        }
    }

    /**
     * Méthode qui arrête le jeu en cours, en remettant à 0 les liste d'aliens et de projectiles pour qu'ils ne soient plus affichés à l'écran
     */
    public void stopJeu(){
        this.armeeAliens=new ArrayList<>();
        this.tirsAliens=new ArrayList<>();
        }
    
    /**
     * Méthode qui vérifie si le joueur à perdu et affiche un écran de défaite si oui
     * @param alien
     */
    public void  checkDefaite(Alien alien){ 
        SCOREFINAL = score.getScore();
        int nbAliensRestants= this.armeeAliens.size();
        if (alien != null){
            if (alien.getPosY()<=12){ // on considère que le joueur a perdu quand les aliens sont très près de lui 
                stopJeu();
                fenetresJFX.GameOver(SCOREFINAL, nbAliensRestants, true);
            }
        }
        if (vaisseau.getVies()==0){
            stopJeu();
            fenetresJFX.GameOver(SCOREFINAL, nbAliensRestants, false);
        }
    }

    /**
     * Méthode qui vérifie si le joueur à gagné et affiche un écran de victoire si oui
     * @param alien
     */
    public void checkVictoire(){
        SCOREFINAL = score.getScore();
        if (this.armeeAliens.isEmpty() && this.vaisseau.getVies()>0){
            this.victoire=true;
            stopJeu();
            fenetresJFX.Victoire(SCOREFINAL);
        }
    }

    /**
     * Méthode qui prend toutes les chaines de toutes les classes affichées à l'écran pour récupérer leur dessin et l'afficher ensuite avec JavaFX
     * @return Un ensembleChaines, l'ensemble de toutes les chaines à afficher
     */
    public EnsembleChaines getChaines(){
        EnsembleChaines chaines = new EnsembleChaines();
        chaines.ajouteChaine(score.getPosX(), score.getPosY(), score.toString());
        chaines.union(vaisseau.getViesVaisseauChaines());
        if (this.vaisseau != null){
            chaines.union(this.vaisseau.getEnsembleChaines());
        }
        if ( this.projectile!=null){
            chaines.union(this.projectile.getEnsembleChaines());
        }
        for(ProjectileAlien proj :this.tirsAliens){
            chaines.union(proj.getEnsembleChaines());
        }
        for (Alien alien:this.armeeAliens){
            chaines.union(alien.getEnsembleChaines());
        }
        return chaines;
    }

    /**
     * Méthode appelée à chaque tour de jeu qui fait évoluer les aliens et les projectiles, incrémente le score et vérifie si le joueur à gagné ou perdu
     */
    public void jouerUnTour(){
        for (Alien a:this.armeeAliens){
            checkDefaite(a);
        }

        this.score.ajoute(1);
        if ( this.projectile != null){
            this.projectile.evolue();
               
            
           Iterator<Alien> listeAliens = this.armeeAliens.iterator();

           while(listeAliens.hasNext()){
                Alien a = listeAliens.next();
                if ( a.contient(projectile.getPosX(), projectile.getPosY())){
                    if (a instanceof AlienTireur){
                        vaisseau.gagneUneVIe();
                        this.score.ajoute(300);
                    }
                    else {
                        this.score.ajoute(100);
                    }
                    this.armeeAliens.remove(a); 
                    this.projectile=null;
                    break;
                    }
           }
           if (this.projectile!=null){
                if ( this.projectile.getPosY()>=60){
                    this.projectile=null;
           }
        }
        
        }
        for (Alien alien:this.armeeAliens){
            alien.evolue();
            alien.tirer(tirsAliens);
            }

        List<ProjectileAlien> projsARemove = new ArrayList<>();
        for (ProjectileAlien proj:this.tirsAliens){
            proj.evolue();
            if (proj.getPosY()<=0){
                projsARemove.add(proj);
            }
            if ( vaisseau.contient(proj.getPosX(), proj.getPosY())){
                vaisseau.perdUneVie();
                projsARemove.add(proj);
                checkDefaite(null);
                }
        }
        this.tirsAliens.removeAll(projsARemove);
        if (this.victoire == false){
            checkVictoire();
        }  
    }
}   


