/**
* @param posX un double représentant la position en X du projectile
* @param posY un double représentant la position en Y du projectile
*/
public class Projectile {
    
    private double posX,posY;

    /**
     * Constructeur de la classe Projectile qui crée un nouveau projectile avec les positions données en paramètres
     * @param posX un double représentant la position en X du projectile
     * @param posY un double représentant la position en Y du projectile 
     */
    public Projectile(double posX, double posY){
        this.posX=posX;
        this.posY=posY;
    }

    /**
     * Méthode qui renvoie un double, la position en X du projectile
     * @return posX
     */
    public double getPosX(){
        return this.posX;
    }
    /**
     * Méthode qui renvoie un double, la position en Y du projectile
     * @return posY
     */
    public double getPosY(){
        return this.posY;
    }

    /**
     * Méthode qui renvoie l'ensemble des chaines du projectile, donc son dessin qui sera affiché dans le jeu
     * @return EnsembleChaines : projectile
     */
    public EnsembleChaines getEnsembleChaines(){
        EnsembleChaines projectile = new EnsembleChaines();
        projectile.ajouteChaine(posX, posY, "┃");

        return projectile;
    }
    /**
     * Méthode qui fait évoluer la position de l'axe Y du projectile à chaque tour de jeu
     */
    public void evolue(){
        this.posY+=0.5;
 }
}

