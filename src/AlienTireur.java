public class AlienTireur extends Alien {
    /**
     * Constructeur de la classe Alien tireur, enfant de la classe Alien.
     * Crée un nouvel alien tireur avec les positions données en paramètres et les attributs de la classe parent
     * @param posX un double représentant la position en X de l'alien
     * @param posY un double représentant la position en Y de l'alien 
     */
    public AlienTireur(double posX, double posY){
        super(posX, posY);
    }

    /**
     * Méthode qui renvoie l'ensemble des chaines du dessin de l'alien tireur. Selon le nombre de tours, elle renvoie un dessin différent pour provoquer une animation de l'alien
     * @return EnsembleChaines : l'ensemble des chaînes du dessin de l'alien tireur avec leur position X et Y sur la fenêtre
     */
    public EnsembleChaines getEnsembleChaines(){ 
        EnsembleChaines alien = new EnsembleChaines();
        if (super.getNbToursAlien()%50<25){            
            alien.ajouteChaine(super.getPosX(), super.getPosY()+6,"               ");
            alien.ajouteChaine(super.getPosX(), super.getPosY()+5,"     ▄███▄     ");
            alien.ajouteChaine(super.getPosX(), super.getPosY()+4,"   ▄███████▄   ");
            alien.ajouteChaine(super.getPosX(), super.getPosY()+3,"  ███▄███▄███  ");
            alien.ajouteChaine(super.getPosX(), super.getPosY()+2,"    ▄▀▄ ▄▀▄    ");
            alien.ajouteChaine(super.getPosX(), super.getPosY()+1,"   ▀ ▀ ▀ ▀ ▀   ");
            alien.ajouteChaine(super.getPosX(), super.getPosY(),  "               ");
        }
        else {
            alien.ajouteChaine(super.getPosX(), super.getPosY()+6,"               ");
            alien.ajouteChaine(super.getPosX(), super.getPosY()+5,"     ▄███▄     ");
            alien.ajouteChaine(super.getPosX(), super.getPosY()+4,"   ▄███████▄   ");
            alien.ajouteChaine(super.getPosX(), super.getPosY()+3,"  ███▄███▄███  ");
            alien.ajouteChaine(super.getPosX(), super.getPosY()+2,"      ▄▀▄      ");
            alien.ajouteChaine(super.getPosX(), super.getPosY()+1,"     ▀ ▀ ▀     ");
            alien.ajouteChaine(super.getPosX(), super.getPosY(),  "               ");
        }                
        return alien;
    }
}
