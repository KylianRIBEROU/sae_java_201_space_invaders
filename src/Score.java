/**
* @param score un entier représentant le score du joueur
* @param x un double représentant la position en X du score
* @param y un double représentant la position en Y du score
*/

public class Score {
    
    private int score;
    private double posX, posY;
    /**
     * Constructeur de la classe Score qui permet de crée un nouveau score avec ses valeurs initialisées
     */
    public Score(){
        this.posX=1;
        this.posY=58;
        this.score=0;
    }
    /**
     * Méthode qui prend en paramètre un entier et l'ajoute au score 
     * @param x un entier 
     */
    public void ajoute(int x){
        this.score+=x;
    }
    /**
     * Méthode qui renvoie un entier, le score 
     * @return score
     */
    public int getScore(){
        return this.score;   
    }
    
    /**
     * Méthode qui renvoie un double, la position en X du Score
     * @return posX
     */
    public double getPosX(){
        return this.posX;
    }
    /**
     * Méthode qui renvoie un double, la position en Y du Score
     * @return posY
     */
    public double getPosY(){
        return this.posY;
    }

    /**
     * Méthode qui renvoie un String, le score mis en forme
     * @return le score et sa valeur
     */
    @Override
    public String toString(){
        return "Score : "+this.getScore();
    }
}
