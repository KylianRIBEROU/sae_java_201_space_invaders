import java.util.ArrayList;
/**
* @param chaines Une liste de ChainePositionnee 
*/

public class EnsembleChaines {
    ArrayList<ChainePositionnee> chaines;

    /**
     * Constructeur de la classe EnsembleChaines qui crée un nouvel EnsembleChaines avec une liste de chaines vides
     */
    public EnsembleChaines(){
        this.chaines= new ArrayList<ChainePositionnee>(); 
    }

    /**
     * Méthode qui crée une chaine positionnée avec les attributs passés en paramètres et l'ajoute à la liste de chaines positionnées
     * @param x un double représentant la position en X de la chaine positionnée
     * @param y un double représentant la position en Y de la chaine positionnée 
     * @param c un String représentant la chaine de caractères de la chaine positionnée
     */
    public void ajouteChaine(double x, double y, String c){
        this.chaines.add(new ChainePositionnee(x,y,c));
    }

    /**
     * Méthode qui prend un ensembleChaines en paramètres et ajoute toutes ses chaines à l'ensembleChaines de l'objet
     * @param e un ensemble de chaines
     */
    public void union(EnsembleChaines e){
        for(ChainePositionnee c : e.chaines)
            this.chaines.add(c);
    }
    /**
     * Méthode qui renvoie un boolean, true si les positions passées en paramètres sont en collision avec des chaines de l'ensemble, false sinon
     * @param posX un double représentant la position en X
     * @param posY un double représentant la position en Y
     * @return boolean
     */
   public boolean contient(double posX, double posY){
       for (ChainePositionnee c: this.chaines){
            if (posX == c.x && posY == c.y){
                return true;
            }    
        }
        return false;
    }
    /**
     * Méthode qui renvoie l'ensemble des chaines de l'ensembleChaines
     */
    @Override
    public String toString(){
        String phrase ="";
        for (ChainePositionnee c: this.chaines){
            phrase += c.toString()+"\n";
        }
        return phrase;
    }

}
