import java.util.List;
/**
     * @param posX un double représentant la position en X de l'alien
     * @param posY un double représentant la position en Y de l'alien
     * @param mort un boolean qui représente si l'alien est mort ou non
     * @param nbTours un entier qui représente le nombre de tours de jeu passés en vie par l'alien depuis le début du jeu 
     */
public class Alien {
    
    private double posX, posY;
    private boolean mort;
    private int nbTours;

    /**
     * Constructeur de la classe Alien qui crée un nouvel objet Alien, avec les attributs donnés en paramètre. par défault, l'alien n'est pas mort
     * et commence à 0 tours de jeu.
     * @param posX un double représentant la position en X de l'alien
     * @param posY un double représentant la position en Y de l'alien 
     */
    public Alien(double posX, double posY){
        this.posX=posX;
        this.posY=posY;
        this.mort=false;
        this.nbTours=0;
    }
    /**
     * Méthode qui renvoie le nombre de tours de jeu passés en vie par l'alien depuis le début du jeu
     * @return nbTours : un entier 
     */
    public int getNbToursAlien(){
        return this.nbTours;
    }
    /**
     * Méthode qui renvoie un boolean qui dit si l'alien est mort ou non.
     * @return mort : un boolean
     */
    public boolean estMort(){
        return this.mort;
    }
    /**
     * Méthode qui permet de définir la valeur de l'attribut mort de l'alien avec la nouvelle valeur passée en paramètre
     * @param estMort un boolean qui dit si l'alien est mort ou non
     */
    public void setMort(boolean estMort){
        this.mort=estMort;
    }
    /**
     * Méthode qui renvoie la position en X de l'alien
     * @return posX : un double 
     */
    public double getPosX(){
        return this.posX;
    }
    /**
     * Méthode qui renvoie la position en Y de l'alien
     * @return posY : un double
     */
    public double getPosY(){
        return this.posY;
    }
    /**
     * Méthode qui renvoie la position sur l'axe X correspondant au milieu du dessin de l'alien
     * @return un double
     */
    public double getPosMilieuAlien(){
        return this.getPosX()+("  ███▄███▄███  ".length()/2);
    }
    /**
     * Méthode qui créer un nouveau projectileAlien a la position du canon de l'alien tous les 100 tours et l'ajoute à la liste des projectiles aliens 
     * @param tirsAliens la liste des projectiles aliens existants
     */
    public void tirer(List<ProjectileAlien> tirsAliens){
        if  (this instanceof AlienTireur && this.nbTours%100==0){
            tirsAliens.add(new ProjectileAlien( this.getPosMilieuAlien(), this.getPosY()-1));
        }
    }
    /**
     * Méthode qui renvoie l'ensemble des chaines qui représentent le dessin de l'alien. Comme tous les aliens sont des enfants de la classe alien
     * et ont leur propre dessin, cette méthode est vide
     * @return Un EnsembleChaines vide
     */
    public EnsembleChaines getEnsembleChaines(){ // cette fonction est appelée à chaque tour, donc le skin change à chaque tour. Il faut régler ça 
       return new EnsembleChaines();
    }

    /**
     * Méthode qui renvoie un boolean, true si les positions passées en paramètres sont en collision avec celles de l'alien, false sinon
     * @param posX un double représentant la position en X
     * @param posY un double représentant la position en Y
     * @return boolean 
     */
    public boolean contient(double posX, double posY){  // le 14 est la taille de l'alien a l"horizontale
        if ( (posX >= this.getPosX() && posX <= this.getPosX()+14) && (posY >= this.getPosY() && posY <= this.getPosY()+8)){
            return true;
        }
        return false;
    }

    /**
     * Méthode qui fait évoluer l'alien à chaque tour de jeu. Selon le nombre de tours passés, il se déplacera différement
     */
    public void evolue(){
        if (this.nbTours < 100){
            this.posX += 0.1;
        }
        if (this.nbTours == 100){
            this.posY -= 2;
        }
        if(this.nbTours > 100){
            this.posX -= 0.1;
        }
        if (this.nbTours == 200){
            this.nbTours = 0;
        }
        this.nbTours+=1;
    }
    
}
